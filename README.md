# create_nfs_server



## Description

This role installing nfs server packets, sharing folder and tuning firewall settings

## Include role

```
- src: https://gitlab.com/ansible_roles_a_whiter/nfs/create_nfs_server.git
  version: "1.0"
  scm: git
  name: nfs_server
```


## Example playbook

```
- hosts: nfs_server
  gather_facts: true
  become: true

  roles:
    - {role: create_nfs_server, when: ansible_system == "Linux"}

```
